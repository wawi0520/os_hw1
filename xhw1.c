/*
 * This implements an application calling the sys_xmergesort system call
 * @file    xhw1.c
 * @author  Wei-Ying Tsai
 * @contact wetsai@cs.stonybrook.edu
 * @date    2016.10.08
 */
#include <asm/unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/syscall.h>
#include <unistd.h>
#include <string.h>
#include "common.h"

#ifndef __NR_xmergesort
#error xmergesort system call not defined
#endif

#ifdef DEBUG_CMD_ARGUMENT
#undef DEBUG_CMD_ARGUMENT
#endif

int user_validate_flags(u_int flags)
{
	if ((flags | OPT_UNIQUE_RECORD | OPT_ALL_RECORDS | OPT_CASE_INSENSITIVE
			| OPT_STOP_IF_FILE_NOT_FOUND | OPT_NUM_RECORD)
		!= (OPT_UNIQUE_RECORD | OPT_ALL_RECORDS | OPT_CASE_INSENSITIVE
			| OPT_STOP_IF_FILE_NOT_FOUND | OPT_NUM_RECORD)) {
		printf("Error: only -u, -a, -i, -d, -t flags are supported\n");
		return -EINVAL;
	}
	if ((flags & OPT_UNIQUE_RECORD) && (flags & OPT_ALL_RECORDS)) {
		printf("Error: you cannot enter both -u and -a flags\n");
		return -EINVAL;
	}
	if ((!(flags & OPT_UNIQUE_RECORD)) && !((flags & OPT_ALL_RECORDS))) {
		printf("Error: you must enter either -u or -a flags\n");
		return -EINVAL;
	}
	return 0; /* success */
}

int main(int argc, char *argv[])
{
	int c;
	int i;
	int rc;
	u_int data = 0;
	struct sys_xmergesort_args sys_call_arg;

	sys_call_arg.flags = 0;
	sys_call_arg.data = &data;
	/* Parse options and set the flag */
	while (-1 != (c = getopt(argc, argv, "uaitd"))) {
		switch (c) {
		case 'u':
			sys_call_arg.flags |= OPT_UNIQUE_RECORD;
			break;
		case 'a':
			sys_call_arg.flags |= OPT_ALL_RECORDS;
			break;
		case 'i':
			sys_call_arg.flags |= OPT_CASE_INSENSITIVE;
			break;
		case 't':
			sys_call_arg.flags |= OPT_STOP_IF_FILE_NOT_FOUND;
			break;
		case 'd':
			sys_call_arg.flags |= OPT_NUM_RECORD;
			break;
		case '?':
			fprintf(stderr, "Unknown option: \"%c\"", optopt);
			fprintf(stderr, "Accepted options are u, a, i, t, d.\n");
			return -1;
		default:
			fprintf(stderr, "Unknown option: \"%c\"", optopt);
			fprintf(stderr, "Accepted options are u, a, i, t, d.\n");
			return -1;
		}
	}
	rc = user_validate_flags(sys_call_arg.flags);
	if (rc != 0) {
		fprintf(stderr, "Invalid options: only u, a, i, t, d are allowed.\n");
		fprintf(stderr, "Also, either u or a should exist, but not both.\n");
		return -1;
	}

	/* Support one output file and up to 10 input files */
	if ((argc - optind) < 3 || (argc - optind) > 11) {
		printf("use:./xmergesort -[uaitd] outfile infile1 infile2 [infile3\n");
		printf("infile4 infile5 infile6 infile7 infile8 infile9 infile10]\n");
		return -1;
	}

	sys_call_arg.num_input_files = argc - optind - 1;
	/* Set output file first */
	sys_call_arg.user_outfilename = argv[optind++];
	if (sys_call_arg.user_outfilename == NULL) {
		fprintf(stderr, "The output file name is NULL.\n");
		rc = -1;
		goto out;
	}
	sys_call_arg.user_infilenames = (char **)calloc(
											sys_call_arg.num_input_files,
											sizeof(char *));
	if (sys_call_arg.user_infilenames == NULL) {
		rc = -1;
		goto out;
	}
	for (i = 0; i < sys_call_arg.num_input_files; ++i) {
		sys_call_arg.user_infilenames[i] = argv[optind++];
		if (sys_call_arg.user_infilenames[i] == NULL) {
			fprintf(stderr, "Some input file name is NULL.\n");
			rc = -1;
			goto out;
		}
	}

#ifdef DEBUG_CMD_ARGUMENT
	printf("Flag: %x.\n", sys_call_arg.flags);
	for (i = 0; i < sys_call_arg.num_input_files; ++i)
		printf("Input filename: %s\n", sys_call_arg.user_infilenames[i]);
	printf("Output filename: %s\n", sys_call_arg.user_outfilename);
#endif

	rc = syscall(__NR_xmergesort, (void *) &sys_call_arg);

	if (rc == 0) {
		/* System call succeeds */
		if (OPT_NUM_RECORD == (sys_call_arg.flags & OPT_NUM_RECORD))
			printf("syscall returned %d. Sort %d records\n", rc,
												(int)(*(sys_call_arg.data)));
		else
			printf("syscall returned %d.\n", rc);
	} else {
		/* System call fails */
		perror("syscall returned error");
	}

out:
	if (sys_call_arg.user_infilenames)
		free(sys_call_arg.user_infilenames);
	exit(rc);
}
