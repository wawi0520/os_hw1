/*
 * This declares/implments commonly-used structure and string utility
 * @file    common.h
 * @author  Wei-Ying Tsai
 * @contact wetsai@cs.stonybrook.edu
 * @date    2016.10.08
 */
#ifndef _COMMON_H_
#define _COMMON_H_

#include <linux/string.h>

#define OPT_UNIQUE_RECORD			(0x01)
#define OPT_ALL_RECORDS				(0x02)
#define OPT_CASE_INSENSITIVE		(0x04)
#define OPT_STOP_IF_FILE_NOT_FOUND	(0x10)
#define OPT_NUM_RECORD				(0x20)

struct sys_xmergesort_args {
	char **user_infilenames; /* input file names (user space) */
	int num_input_files;     /* number of input files */
	char *user_outfilename;  /* output file name (user space) */
	u_int flags;             /* flag given by the user */
	u_int *data;             /* number of recoreds sorted */
};

/* Compare two
 * @param [in] str1 The first string
 * @param [in] len1 The length of the first string
 * @param [in] str2 The second string
 * @param [in] len2 The length of the second string
 * @param [in] case_sensitive Type of comparison
 */
int compare_strings(
	char *str1,
	ssize_t len1,
	char *str2,
	ssize_t len2,
	ushort case_sensitive
	)
{
	ssize_t length = (len1 > len2) ? len1 : len2;

	if (case_sensitive != 0)
		return strncmp(str1, str2, length);
	else
		return strncasecmp(str1, str2, length);
}

#endif
