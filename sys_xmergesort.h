/*
 * This declares/implments commonly-used structure and string utility
 * @file    sys_xmergesort.h
 * @author  Wei-Ying Tsai
 * @contact wetsai@cs.stonybrook.edu
 * @date    2016.10.08
 */
#ifndef _SYS_XMERGESORT_H_
#define _SYS_XMERGESORT_H_

#include <linux/fs.h>
#include <linux/types.h>

/* Check if the flag is legal
 * @param [in] flags The flag given by the user
 * @return 0 if success; negative value otherwise
 */
inline int validate_flags(u_int flags);

/* Check if the given file name is ok.
 * @param [in] file_name The file name being checked
   @return 0 if success; negative otherwise;
 */
inline int check_file_path(char *file_name);

/* Make sure all input and output files are different
 * @param [in] inFp An array of input file pointers
 * @param [in] num_input_files The number of input files
 * @param [in] outFp The output file pointer
 * @return 0 if success; negative value otherwise
 */
int ensure_files_different(
	struct file *inFp[],
	int num_input_files,
	struct file *outFp
	);

/* Check if all input files have been read to an end
 * @param [out] complete Indicates which file is read completely
 * @param [in] size The number of input files
 */
bool allComplete(bool *complete, int size);

/* Get the start and the end index of a record delimited by newline
 * @param [in] buffer The buffer containing records
 * @param [in] buf_size The size of the buffer
 * @param [out] begin The start index of a record
 * @param [out] end The end index of a record
 */
bool get_indexes(
	char *buffer,
	ssize_t buf_size,
	ssize_t *begin,
	ssize_t *end
	);

/*
 * Warpper function for reading file
 * @param [in] fp The file pointer
 * @param [in/out] buffer The buffer where the file content is stored
 * @param [in/out] complete Indicate if the file has been all read
 * @param [in] read_size The maximum number of bytes being read
 * @return The number of bytes read from file if positive; error if negative
 */
ssize_t my_read_file_wrapper(
	struct file *fp,
	char *buffer,
	bool *complete,
	size_t read_size
	);

/*
 * Warpper function for writing file
 * @param [in/out] fp The file pointer
 * @param [in] buffer The buffer that will be written into file
 * @param [in] write_size The number of bytes will be written to file
 * @return The number of bytes written to file if positive; error if negative
 */
inline int my_write_file_wrapper(
	struct file *fp,
	char *buffer,
	size_t write_size
	);

/*
 * Warpper function for unlinking file
 * @param [in/out] fp The file pointer
 */
void my_delete_file_wrapper(struct file *fp);

/*
 * Warpper function for renaming file
 * @param [in] src_fp The source file pointer
 * @param [in] dest_fp The destination file pointer
 * @return 0 if success; negative value otherwise
 */
int my_rename_file_wrapper(struct file *src_fp, struct file *dest_fp);

#endif
