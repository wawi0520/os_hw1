/*
 * This implements the merge sort system call
 * @file    sys_xmergesort.c
 * @author  Wei-Ying Tsai
 * @contact wetsai@cs.stonybrook.edu
 * @date    2016.10.08
 */
#include <linux/fcntl.h>
#include <linux/fs.h>
#include <linux/limits.h>
#include <linux/linkage.h>
#include <linux/moduleloader.h>
#include <linux/namei.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include "common.h"
#include "linked_list.h"
#include "sys_xmergesort.h"

#ifdef DEBUG_KERNEL_DETAIL
#undef DEBUG_KERNEL_DETAIL
#endif

#ifdef DEBUG_WRITE_FILE
#undef DEBUG_WRITE_FILE
#endif

#define MAX_NUM_OF_FILES 10
#define TEMP_FILENAME_PREFIX "wetsai_temp_"
#define MAX_TEMP_FILENAME_SUFFIX 2
#define MAX_TEMP_FILE 100

asmlinkage extern long (*sysptr)(void *arg);

asmlinkage long xmergesort(void *arg)
{
	int i; /* for loop */
	/* error number. Zero by default and will be overwritten if errors occur */
	int errno = 0;
	int ret = 0;
	/* Specify the flag copied from the user space */
	u_int kernel_flags = 0;
	/* Specify the number of input files copied from user space */
	int kernel_num_input_files = 0;
	/* Specify the number of bytes read from each file */
	ssize_t *bytesRead = NULL;
	/* Specify the number of valid input files */
	int num_valid_input_files = 0;
	/* Input file names copied from user space */
	char **kernel_infilenames = NULL;
	/* Temporary output file name */
	char temp_output_filename[NAME_MAX] = {'\0'};
	/* Specify output file name */
	char *kernel_outfilename = NULL;
	/* An array of file pointers */
	struct file *inFp[MAX_NUM_OF_FILES] = {NULL};
	/* The start and end index specify the start and the end of a record */
	ssize_t **index = NULL;
	/* Indicate if an input buffer has been read completely. If so, we need
	 * to read thefile content into the buffer
	 */
	bool buffer_read_complete = false;
	/* A linked list head */
	struct ListNode *head = NULL;
	struct ListNode *node = NULL;
	/* Indicate whether the input buffer has been processed */
	bool *bufferHasBeenRead = NULL;
	/* Indicate if a file has been all read */
	bool *file_complete = NULL;
	/* The output buffer that waits for being written to the file */
	char *write_buffer = NULL;
	/* The length of the write buffer */
	u_int write_buffer_len = 0;
	/* Specify the file pointer to the temporary output file */
	struct file *temp_outFp = NULL;
	/* Specify the file pointer to the output file */
	struct file *outFp = NULL;
	/* The input buffers written from the files */
	char **kernel_input_buffer = NULL;
	/* Specify the record that is just sorted */
	char *last_output_record = NULL;
	/* Specify the total number of records sorted */
	u_int num_of_records = 0;
	bool new_output_file_created = false;
	/* Indicate case senstive or insenstive */
	ushort case_sensitive = 1;
	bool first_time_check_duplicate = true;
	/* Specify the permission of the output file */
	umode_t mode = (current->mm->exe_file->f_path.dentry->d_inode->i_mode)
					& (~S_IXUSR) & (~S_IXGRP) & (~S_IXOTH);
	struct sys_xmergesort_args *args = (struct sys_xmergesort_args *)arg;
#ifdef DEBUG_KERNEL_DETAIL
	pr_info("file:%d, flag:%d, data:%d\n", args->num_input_files,
		args->flags, *(args->data));
#endif

	copy_from_user(&kernel_flags, &args->flags, sizeof(args->flags));
	copy_from_user(&kernel_num_input_files, &args->num_input_files,
					sizeof(args->num_input_files));

	/* Check flags */
	errno = validate_flags(kernel_flags);
	if (errno != 0)
		goto RECYCLE_AND_RETURN;

	if (OPT_CASE_INSENSITIVE == (kernel_flags & OPT_CASE_INSENSITIVE))
		case_sensitive = 0;

	if (kernel_num_input_files > MAX_NUM_OF_FILES) {
		errno = -EINVAL;
		pr_err("Maximum number of input files is 10.\n");
		goto RECYCLE_AND_RETURN;
	}
	for (i = 0; i < kernel_num_input_files; ++i) {
		if (args->user_infilenames[i] == NULL) {
			errno = -EINVAL;
			pr_alert("Check the given input filenames. Are they NULL?\n");
			goto RECYCLE_AND_RETURN;
		}

		/* Check the given paramenter is legal/accessible in the kernel */
		if (!access_ok(VERIFY_READ, args->user_infilenames[i], 0)) {
			errno = -EFAULT;
			pr_alert("Invalid input file from the user space\n");
			goto RECYCLE_AND_RETURN;
		}
		errno = check_file_path(args->user_infilenames[i]);
		if (errno < 0)
			goto RECYCLE_AND_RETURN;
	}
	if (args->user_outfilename == NULL) {
		errno = -EINVAL;
		pr_alert("Check the given output filename. Are they NULL?\n");
		goto RECYCLE_AND_RETURN;
	}
	if (!access_ok(VERIFY_READ, args->user_outfilename, 0)) {
		errno = -EFAULT;
		pr_alert("Invalid output file from the user space\n");
		goto RECYCLE_AND_RETURN;
	}
	errno = check_file_path(args->user_outfilename);
	if (errno < 0)
		goto RECYCLE_AND_RETURN;
	if (OPT_NUM_RECORD == (kernel_flags & OPT_NUM_RECORD)) {
		if (args->data == NULL) {
			errno = -EINVAL;
			pr_alert("Error: data is NULL\n");
			goto RECYCLE_AND_RETURN;
		}
		if (!access_ok(VERIFY_READ, args->data, 0)) {
			errno = -EFAULT;
			pr_alert("Invalid data field from the user space\n");
			goto RECYCLE_AND_RETURN;
		}
	}

	/* Copy filenames from the user space to kernel space */
	kernel_infilenames = kmalloc(
						kernel_num_input_files * sizeof(char *), GFP_KERNEL);
	if (kernel_infilenames == NULL) {
		errno = -ENOMEM;
		goto CLEAR_OUTPUT_FILE;
	} else {
		for (i = 0; i < kernel_num_input_files; ++i)
			kernel_infilenames[i] = NULL;
	}
	for (i = 0; i < kernel_num_input_files; ++i) {
		kernel_infilenames[i] = kmalloc(
					sizeof(char) * strlen_user(args->user_infilenames[i]) + 1,
					GFP_KERNEL);
		if (kernel_infilenames[i] == NULL) {
			errno = -ENOMEM;
			goto RECYCLE_AND_RETURN;
		}
		ret = strncpy_from_user(
								kernel_infilenames[i],
								args->user_infilenames[i],
								strlen_user(args->user_infilenames[i]));
		if (ret < 0) {
			errno = -EFAULT;
			goto RECYCLE_AND_RETURN;
		}
		kernel_infilenames[i][strlen_user(args->user_infilenames[i])] = '\0';
	}
	kernel_outfilename = kmalloc(
						sizeof(char) * strlen_user(args->user_outfilename) + 1,
						GFP_KERNEL);
	if (kernel_outfilename == NULL) {
		errno = -ENOMEM;
		goto RECYCLE_AND_RETURN;
	}
	ret = strncpy_from_user(kernel_outfilename, args->user_outfilename,
								strlen_user(args->user_outfilename));
	if (ret < 0) {
		errno = -EFAULT;
		goto RECYCLE_AND_RETURN;
	}
	kernel_outfilename[strlen_user(args->user_outfilename)] = '\0';

	for (i = 0; i < kernel_num_input_files; ++i) {
		inFp[num_valid_input_files] = filp_open(kernel_infilenames[i],
												O_RDONLY, 0);
		if (!inFp[num_valid_input_files]
				|| IS_ERR(inFp[num_valid_input_files])) {
			pr_alert("The input file %s is not found\n",
					kernel_infilenames[num_valid_input_files]);
			errno = -EINVAL;
			goto RECYCLE_AND_RETURN;
		}

		if (!S_ISREG(inFp[num_valid_input_files]->f_path.dentry->
				d_inode->i_mode)) {
			pr_alert("The input file %s is not a regular file.\n",
					kernel_infilenames[num_valid_input_files]);
			errno = -EINVAL;
			goto RECYCLE_AND_RETURN;
		}
		mode &= (inFp[num_valid_input_files]->f_path.dentry->d_inode->i_mode);
		/* The file exists. */
		++num_valid_input_files;
	}

	/* Construct temporary output file name and create it*/
	i = 0;
	strncpy(temp_output_filename, TEMP_FILENAME_PREFIX,
			strlen(TEMP_FILENAME_PREFIX));
	while (1) {
		/* sprintf will automatically append the NULL character */
		sprintf(temp_output_filename + strlen(TEMP_FILENAME_PREFIX), "%03d", i);
		temp_outFp = filp_open(temp_output_filename,
								O_WRONLY | O_CREAT | O_EXCL, 0);
		if (!temp_outFp || IS_ERR(temp_outFp)) {
			errno = (int)PTR_ERR(temp_outFp);
			if (EEXIST != -errno) {
				pr_alert("The temp output file %s can't be created\n",
						temp_output_filename);
				goto RECYCLE_AND_RETURN;
			} else {
				++i;
				if (i == MAX_TEMP_FILE) {
					pr_alert(
						"Maximum trial of creating temp file is reached.\n");
					goto RECYCLE_AND_RETURN;
				}
			}
		} else{
			/* Successfully create the temp file. Set the owner/group as that
			 * of the current process, and then set the permission.
			 */
			i_uid_write(temp_outFp->f_path.dentry->d_inode,
						current->real_cred->uid.val);
			i_gid_write(temp_outFp->f_path.dentry->d_inode,
						current->real_cred->gid.val);
			temp_outFp->f_path.dentry->d_inode->i_mode = mode;
			break;
		}
	}
	/* We first try to open the file. If there is an error code ENOENT, then
	 * we create a new one. The reason we use the two-step opening file is
	 * that we don't want to affect the original file if this syscall fails.
	 */

	/* Open or create the actual output file. Needn't include O_EXCL */
	outFp = filp_open(kernel_outfilename, O_WRONLY, 0);
	if (!outFp || IS_ERR(outFp)) {
		errno = (int)PTR_ERR(outFp);
		if (ENOENT == -errno) {
			/* If no such file, create a new one. Need include O_EXCL */
			outFp = filp_open(kernel_outfilename,
								O_WRONLY | O_CREAT | O_EXCL, 0);
			if (!outFp || IS_ERR(outFp)) {
				errno = (int)PTR_ERR(outFp);
				pr_alert("The output file %s cannot be created\n",
						kernel_outfilename);
				goto CLEAR_OUTPUT_FILE;
			}
			/* The output file has been created.
			 * Let's check if it's a regular file
			 */
			if (!S_ISREG(outFp->f_path.dentry->d_inode->i_mode)) {
				pr_alert("The output file %s isn't a regular file.\n",
						kernel_outfilename);
				errno = -EINVAL;
				goto CLEAR_OUTPUT_FILE;
			}
			/* Turn on new_output_file_created. So that if our syscall
			 * fails, we can delete the file
			 */
			new_output_file_created = true;
		} else {
			pr_alert("The output file %s cannot be created.\n",
					kernel_outfilename);
			errno = -EINVAL;
			goto CLEAR_OUTPUT_FILE;
		}
	} else {
		/* The file already exists. Let's check if it's a regular file. */
		if (!S_ISREG(outFp->f_path.dentry->d_inode->i_mode)) {
			pr_alert("The output file %s is not a regular file.\n",
					kernel_outfilename);
			errno = -EINVAL;
			goto CLEAR_OUTPUT_FILE;
		}
	}

	errno = ensure_files_different(inFp, num_valid_input_files, outFp);
	if (errno != 0)
		goto CLEAR_OUTPUT_FILE;

	bytesRead = kmalloc_array(num_valid_input_files, sizeof(ssize_t),
									GFP_KERNEL);
	if (bytesRead == NULL) {
		errno = -ENOMEM;
		goto CLEAR_OUTPUT_FILE;
	}
	bufferHasBeenRead = kmalloc_array(num_valid_input_files, sizeof(bool),
									GFP_KERNEL);
	if (bufferHasBeenRead == NULL) {
		errno = -ENOMEM;
		goto CLEAR_OUTPUT_FILE;
	}
	for (i = 0; i < num_valid_input_files; ++i)
		bufferHasBeenRead[i] = false;

	file_complete = kmalloc_array(num_valid_input_files, sizeof(bool),
									GFP_KERNEL);
	if (file_complete == NULL) {
		errno = -ENOMEM;
		goto CLEAR_OUTPUT_FILE;
	}
	for (i = 0; i < num_valid_input_files; ++i)
		file_complete[i] = false;

	index = kmalloc_array(num_valid_input_files, sizeof(ssize_t *), GFP_KERNEL);
	if (index == NULL) {
		errno = -ENOMEM;
		goto CLEAR_OUTPUT_FILE;
	} else {
		for (i = 0; i < num_valid_input_files; ++i)
			index[i] = NULL;
	}
	for (i = 0; i < num_valid_input_files; ++i) {
		index[i] = kmalloc(sizeof(ssize_t) * 2, GFP_KERNEL);
		if (index[i] == NULL) {
			errno = -ENOMEM;
			goto CLEAR_OUTPUT_FILE;
		}
		index[i][0] = 0;
		index[i][1] = 0;
	}

	kernel_input_buffer = kmalloc_array(num_valid_input_files, sizeof(char *),
									GFP_KERNEL);
	if (kernel_input_buffer == NULL) {
		errno = -ENOMEM;
		goto CLEAR_OUTPUT_FILE;
	} else {
		for (i = 0; i < num_valid_input_files; ++i)
			kernel_input_buffer[i] = NULL;
	}
	for (i = 0; i < num_valid_input_files; ++i) {
		kernel_input_buffer[i] = kmalloc(sizeof(char) * PAGE_SIZE, GFP_KERNEL);
		if (kernel_input_buffer[i] == NULL) {
			errno = -ENOMEM;
			goto CLEAR_OUTPUT_FILE;
		}
		memset(kernel_input_buffer[i], '\0', PAGE_SIZE);
		bytesRead[i] = 0;
		inFp[i]->f_pos = 0;		/* start offset */
	}
	write_buffer = kmalloc(sizeof(char) * PAGE_SIZE, GFP_KERNEL);
	if (write_buffer == NULL) {
		errno = -ENOMEM;
		goto CLEAR_OUTPUT_FILE;
	}
	memset(write_buffer, '\0', PAGE_SIZE);

	temp_outFp->f_pos = 0;

	last_output_record = kmalloc(sizeof(char) * PAGE_SIZE, GFP_KERNEL);
	if (last_output_record == NULL) {
		errno = -ENOMEM;
		goto CLEAR_OUTPUT_FILE;
	}
	memset(last_output_record, '\0', PAGE_SIZE);

	head = create_node(-1);
	if (head == NULL) {
		errno = -ENOMEM;
		goto CLEAR_OUTPUT_FILE;
	}

	/* Read files first time and sort */
	for (i = 0; i < num_valid_input_files; ++i) {
		while (false == bufferHasBeenRead[i]) {
			buffer_read_complete = get_indexes(kernel_input_buffer[i],
												bytesRead[i],
												&index[i][0],
												&index[i][1]);
			if (true == buffer_read_complete) {
				/* We don't read anything from the buffer. Then we need to
				 * read the file to fill the buffer, and read buffer again.
				 */
				bytesRead[i] = my_read_file_wrapper(inFp[i],
												&kernel_input_buffer[i][0],
												&file_complete[i],
												PAGE_SIZE);
				if (bytesRead[i] < 0) {
					pr_alert("Error: Read from %s fails.\n",
							kernel_infilenames[i]);
					errno = bytesRead[i];
					goto CLEAR_OUTPUT_FILE;
				}
				index[i][0] = 0; /* Reset the start index */
				if (bytesRead[i] == 0) {
					/* Reach the end of the file */
					break;
				}
			} else {
				bufferHasBeenRead[i] = true;
			}
		}
		if (bytesRead[i] == 0) {
			/* Nothing in the file. We just skip processing this file. */
			continue;
		} else {
			/* Create a node */
			node = create_node(i);
			if (node == NULL)
				goto CLEAR_OUTPUT_FILE;
			insert_node(head, node, &kernel_input_buffer[0], index,
						case_sensitive);
		}
	}

	while (false == allComplete(&file_complete[0], num_valid_input_files)) {
		node = extract_min_node(head);
		if (node == NULL) {
			/* We have completed processing all nodes. */
			goto CLEAR_OUTPUT_FILE;
		} else {
			if (OPT_UNIQUE_RECORD != (kernel_flags & OPT_UNIQUE_RECORD)
				|| true == first_time_check_duplicate
				|| 0 != compare_strings
						(
						last_output_record,
						strlen(last_output_record),
						kernel_input_buffer[node->index]+index[node->index][0],
						index[node->index][1] - index[node->index][0] + 1,
						case_sensitive
						)
					) {
				first_time_check_duplicate = false;
				num_of_records++;
				/* Write output data
				 * The length of the wait-to-be-written record is
				 * index[node->index][1] - index[node->index][0] + 1 AND PLUS
				 * ONE because we need to fill the newline '\n'.
				 */
				if ((PAGE_SIZE - write_buffer_len)
					< (index[node->index][1] - index[node->index][0] + 2)) {
					/* Write file */
					ret = my_write_file_wrapper(temp_outFp, write_buffer,
												write_buffer_len);
					if (ret < 0) {
						errno = ret;
						pr_alert("Write error\n");
						goto CLEAR_OUTPUT_FILE;
					}
				#ifdef DEBUG_WRITE_FILE
					pr_debug("Write file:\n");
					for (i = 0; i < write_buffer_len; ++i)
						pr_debug("%c", write_buffer[i]);
				#endif
					memset(write_buffer, '\0', PAGE_SIZE);
					write_buffer_len = 0;
				}
				/* include newline, so the length will be the length of records
				 *plus 1.
				 */
				strncpy
					(
					write_buffer + write_buffer_len,
					kernel_input_buffer[node->index] + index[node->index][0],
					index[node->index][1] - index[node->index][0] + 2
					);
				write_buffer_len += (index[node->index][1]
										- index[node->index][0] + 2);

				strncpy
					(
					last_output_record,
					kernel_input_buffer[node->index] + index[node->index][0],
					index[node->index][1] - index[node->index][0] + 1
					);
				/* Make it null termiated */
				last_output_record[index[node->index][1]
										- index[node->index][0] + 1] = '\0';
			}

			/* Update buffer status */
			bufferHasBeenRead[node->index] = false;
			index[node->index][0] = index[node->index][1] + 2;
			index[node->index][1] = index[node->index][0];
		}
		while (false == bufferHasBeenRead[node->index]) {
			buffer_read_complete = get_indexes(
											kernel_input_buffer[node->index],
											bytesRead[node->index],
											&index[node->index][0],
											&index[node->index][1]);
			if (true == buffer_read_complete) {
				/* We don't read anything from the buffer. Then we need to
				 * read the file to fill the buffer, and read buffer again.
				 */
				bytesRead[node->index] = my_read_file_wrapper(
										  inFp[node->index],
										  &kernel_input_buffer[node->index][0],
										  &file_complete[node->index],
										  PAGE_SIZE);
				if (bytesRead[node->index] < 0) {
					pr_alert("Error: Read from %s fails.\n",
						kernel_infilenames[node->index]);
					errno = bytesRead[node->index];
					goto CLEAR_OUTPUT_FILE;
				}
				index[node->index][0] = 0; /* Reset the start index */
				if (bytesRead[node->index] == 0) {
					/* Reach the end of the file */
					break;
				}
			} else {
				ret = compare_strings(
						last_output_record,
						strlen(last_output_record),
						kernel_input_buffer[node->index]+index[node->index][0],
						index[node->index][1] - index[node->index][0] + 1,
						case_sensitive
						);
				/* First check if the order is correct. */
				if (ret > 0) {
					if (OPT_STOP_IF_FILE_NOT_FOUND ==
							(kernel_flags & OPT_STOP_IF_FILE_NOT_FOUND)) {
						pr_alert("Contents in %s is not sorted\n",
								kernel_infilenames[node->index]);
						errno = -EINVAL;
						goto CLEAR_OUTPUT_FILE;
					} else {
						/* An unsorted record is found. Just skip it and then
						 * update the indexes
						 */
						index[node->index][0] = index[node->index][1] + 2;
						index[node->index][1] = index[node->index][0];
						continue;
					}
				}
				/* We do read some data, but skip the data if it duplicates */
				if (OPT_UNIQUE_RECORD != (kernel_flags & OPT_UNIQUE_RECORD)
						|| 0 != ret) {
					bufferHasBeenRead[node->index] = true;
				} else {
					/* Duplicate, so update the indexes. */
					index[node->index][0] = index[node->index][1] + 2;
					index[node->index][1] = index[node->index][0];
				}
			}
		}

		if (bytesRead[node->index] == 0) {
			/* Nothing in the file, so we don't need the node anymore. */
			kfree(node);
		} else {
			insert_node(head, node, &kernel_input_buffer[0], index,
							case_sensitive);
		}
	}
	if (write_buffer_len != 0) {
		ret = my_write_file_wrapper(temp_outFp, write_buffer, write_buffer_len);
		if (ret < 0) {
			errno = ret;
			pr_alert("Write error\n");
			goto CLEAR_OUTPUT_FILE;
		}
	#ifdef DEBUG_WRITE_FILE
		for (i = 0; i < write_buffer_len; ++i)
			pr_debug("%c", write_buffer[i]);
	#endif
}
#ifdef DEBUG_WRITE_FILE
	pr_debug("Write file End\n");
#endif

	errno = my_rename_file_wrapper(temp_outFp, outFp);
	if (errno < 0)
		goto CLEAR_OUTPUT_FILE;
#ifdef DEBUG_KERNEL_DETAIL
	pr_debug("# records: %d\n", num_of_records);
#endif
	if (OPT_NUM_RECORD == (kernel_flags & OPT_NUM_RECORD)) {
		/* Return the number of records */
		copy_to_user(args->data, &num_of_records, sizeof(u_int));
	}

	goto RECYCLE_AND_RETURN;

CLEAR_OUTPUT_FILE:
	my_delete_file_wrapper(temp_outFp);
	/* If new_output_file_created is zero, that means there is already an
	 * file before we execute the system call. So we don't delete original file.
	 */
	if (true == new_output_file_created)
		my_delete_file_wrapper(outFp);

RECYCLE_AND_RETURN:
	if (head) {
		struct ListNode *cur = head;
		struct ListNode *next = NULL;

		while (cur != NULL) {
			next = cur->next;
			kfree(cur);
			cur = next;
		}
		head = NULL;
	}
	kfree(bytesRead);
	bytesRead = NULL;

	if (index) {
		for (i = 0; i < kernel_num_input_files; ++i) {
			kfree(index[i]);
			index[i] = NULL;
		}
		kfree(index);
		index = NULL;
	}
	kfree(bufferHasBeenRead);
	bufferHasBeenRead = NULL;

	kfree(file_complete);
	file_complete = NULL;

	kfree(last_output_record);
	last_output_record = NULL;

	if (kernel_infilenames) {
		for (i = 0; i < kernel_num_input_files; ++i) {
			kfree(kernel_infilenames[i]);
			kernel_infilenames[i] = NULL;
		}
		kfree(kernel_infilenames);
		kernel_infilenames = NULL;
	}

	kfree(kernel_outfilename);
	kernel_outfilename = NULL;

	if (kernel_input_buffer) {
		for (i = 0; i < num_valid_input_files; ++i) {
			kfree(kernel_input_buffer[i]);
			kernel_input_buffer[i] = NULL;
		}
		kfree(kernel_input_buffer);
		kernel_input_buffer = NULL;
	}

	kfree(write_buffer);
	write_buffer = NULL;

	for (i = 0; i < MAX_NUM_OF_FILES; ++i) {
		if (inFp[i] && !IS_ERR(inFp[i])) {
			filp_close(inFp[i], NULL);
			inFp[i] = NULL;
		}
	}
	if (outFp && !IS_ERR(outFp)) {
		filp_close(outFp, NULL);
		outFp = NULL;
	}
	if (temp_outFp && !IS_ERR(temp_outFp)) {
		filp_close(temp_outFp, NULL);
		temp_outFp = NULL;
	}
	return errno; /* 0 if success */
}

int validate_flags(u_int flags)
{
	if ((flags | OPT_UNIQUE_RECORD | OPT_ALL_RECORDS | OPT_CASE_INSENSITIVE
			| OPT_STOP_IF_FILE_NOT_FOUND | OPT_NUM_RECORD)
		!= (OPT_UNIQUE_RECORD | OPT_ALL_RECORDS | OPT_CASE_INSENSITIVE
			| OPT_STOP_IF_FILE_NOT_FOUND | OPT_NUM_RECORD)) {
		pr_alert("Error: only -u, -a, -i, -d, -t flags are supported\n");
		return -EINVAL;
	}
	if ((flags & OPT_UNIQUE_RECORD) && (flags & OPT_ALL_RECORDS)) {
		pr_alert("Error: you cannot enter both -u and -a flags\n");
		return -EINVAL;
	}
	if ((!(flags & OPT_UNIQUE_RECORD)) && !((flags & OPT_ALL_RECORDS))) {
		pr_alert("Error: you must enter either -u or -a flags\n");
		return -EINVAL;
	}
	return 0; /* success */
}

int check_file_path(char *file_name)
{
	struct filename *tmp = getname(file_name);

	if (IS_ERR(tmp))
		return (int)(PTR_ERR(tmp));
	putname(tmp);
	return 0;
}

int ensure_files_different(
	struct file *inFp[],
	int num_input_files,
	struct file *outFp
	)
{
	int i = 0;
	int j = 0;

	for (i = 0; i < (num_input_files - 1); ++i) {
		for (j = i + 1; j < num_input_files; ++j) {
			if (inFp[i]->f_path.dentry->d_inode->i_sb
					== inFp[j]->f_path.dentry->d_inode->i_sb
				&& inFp[i]->f_path.dentry->d_inode->i_ino
					== inFp[j]->f_path.dentry->d_inode->i_ino) {
				pr_alert("Duplicate files: %s\n",
						inFp[i]->f_path.dentry->d_iname);
				return -EINVAL;
			}
		}
	}
	/* check against the output file */
	for (i = 0; i < num_input_files; ++i) {
		if (inFp[i]->f_path.dentry->d_inode->i_sb
				== outFp->f_path.dentry->d_inode->i_sb
			&& inFp[i]->f_path.dentry->d_inode->i_ino
				== outFp->f_path.dentry->d_inode->i_ino) {
			pr_alert("Duplicate files: %s\n", inFp[i]->f_path.dentry->d_iname);
			return -EINVAL;
		}
	}
	return 0;
}

ssize_t my_read_file_wrapper(
	struct file *fp,
	char *buffer,
	bool *complete,
	size_t read_size
	)
{
	mm_segment_t oldfs;
	int i; /* for loop */
	ssize_t truncated_size = 0;
	ssize_t bytesRead = 0;

	memset(buffer, '\0', PAGE_SIZE);
	oldfs = get_fs();
	set_fs(KERNEL_DS);
	bytesRead = vfs_read(fp, buffer, read_size, &fp->f_pos);
	if (bytesRead < 0) /* Error */
		return bytesRead;
	set_fs(oldfs);
	if (bytesRead == 0 || (bytesRead == 1 && buffer[0] == '\n')) {
		*complete = true;
		return 0; /* Nothing is read. */
	}
	/* Find last occurrence of \n. Note: don't use strrchr to find the last
	 *  occurrence of \n because the buffer may not be not NULL-terminated.
	 */
	for (i = bytesRead - 1; i >= 0 && buffer[i] != '\n'; --i)
		;
	truncated_size = (i + 1);
	fp->f_pos = fp->f_pos - (bytesRead-truncated_size);
	return truncated_size;
}

int my_write_file_wrapper(
	struct file *fp,
	char *buffer,
	size_t write_size
	)
{
	int ret;
	mm_segment_t oldfs = get_fs();

	set_fs(KERNEL_DS);
	ret = vfs_write(fp, buffer, write_size, &fp->f_pos);
	set_fs(oldfs);
	return ret;
}

void my_delete_file_wrapper(struct file *fp)
{
	struct inode *inode = d_inode(fp->f_path.dentry);
	struct inode *dirp;

	ihold(inode);
	dirp = d_inode(fp->f_path.dentry->d_parent);
	vfs_unlink(dirp, fp->f_path.dentry, NULL);
	if (inode)
		iput(inode);
}

int my_rename_file_wrapper(struct file *src_fp, struct file *dest_fp)
{
	int rc = 0;
	struct dentry *trap = NULL;
	struct dentry *old_dir_dentry = dget_parent(src_fp->f_path.dentry);
	struct dentry *new_dir_dentry = dget_parent(dest_fp->f_path.dentry);

	trap = lock_rename(old_dir_dentry, new_dir_dentry);
	/* source should not be ancestor of target */
	if (trap == old_dir_dentry) {
		rc = -EINVAL;
		goto out_lock;
	}
	/* target should not be ancestor of source */
	if (trap == new_dir_dentry) {
		rc = -ENOTEMPTY;
		goto out_lock;
	}

	rc = vfs_rename(d_inode(old_dir_dentry), src_fp->f_path.dentry,
			d_inode(new_dir_dentry), dest_fp->f_path.dentry, NULL, 0);
out_lock:
	unlock_rename(old_dir_dentry, new_dir_dentry);
	dput(old_dir_dentry);
	dput(new_dir_dentry);
	return rc;
}

bool allComplete(bool *complete, int size)
{
	int i;

	for (i = 0; i < size; ++i) {
		if (false == complete[i])
			return false;
	}
	return true;
}

bool get_indexes(
	char *buffer,
	ssize_t buf_size,
	ssize_t *begin,
	ssize_t *end
	)
{
	ssize_t i;

	for (i = *begin; i < buf_size; ++i) {
		if ('\n' == buffer[i]) {
			*end = i - 1;
			break;
		}
	}
	if (buf_size <= *begin)
		return true;
	else
		return false;
}

static int __init init_sys_xmergesort(void)
{
	pr_info("installed new sys_xmergesort module\n");
	if (sysptr == NULL)
		sysptr = xmergesort;
	return 0;
}
static void  __exit exit_sys_xmergesort(void)
{
	if (sysptr != NULL)
		sysptr = NULL;
	pr_info("removed sys_xmergesort module\n");
}
module_init(init_sys_xmergesort);
module_exit(exit_sys_xmergesort);
MODULE_LICENSE("GPL");
