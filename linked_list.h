/*
 * This delcares/implements linked list
 * @file    linked_list.h
 * @author  Wei-Ying Tsai
 * @contact wetsai@cs.stonybrook.edu
 * @date    2016.10.08
 */
#ifndef _LINK_LIST_H_
#define _LINK_LIST_H_

#include <linux/printk.h>
#include <linux/slab.h>
#include "common.h"

/* The linked list node
 * The data stored in the node is an index to which input buffer.
 */
struct ListNode {
	int index;
	struct ListNode *next;
};

/* Create a linked list node with a given index
 * @param [in] i An index to which input buffer
 * @return A newly-created node
 */
struct ListNode *create_node(int i)
{
	struct ListNode *node = kmalloc(
									sizeof(struct ListNode), GFP_KERNEL);

	node->index = i;
	node->next = NULL;
	return node;
}

/* Insert a node to the linked list in a sorted order
 * @param [in] head The heading node of the list
 * @param [in] node The node being inserted
 * @param [in] buffer The input buffers
 * @param [in] index Indexes to the start/end index of a record
 * @param [in] case_sensitive Specify case sensitive or insensitive
*/
void insert_node(
	struct ListNode *head,
	struct ListNode *node,
	char **buffer,
	ssize_t **index,
	ushort case_sensitive
	)
{
	struct ListNode *prev = NULL;
	struct ListNode *cur = NULL;

	if (head == NULL)
		return;
	prev = head;
	cur = head->next;
	while (cur != NULL) {
		int res = compare_strings
					(
					buffer[cur->index] + index[cur->index][0],
					index[cur->index][1] - index[cur->index][0] + 1,
					buffer[node->index] + index[node->index][0],
					index[node->index][1] - index[node->index][0] + 1,
					case_sensitive
					);

		if (res > 0) {
			/* The first string is greater than the second one */
			prev->next = node;
			node->next = cur;
			prev = node;
			break;
		}
		prev = prev->next;
		cur = cur->next;
	}
	if (cur == NULL) {
		prev->next = node;
		node->next = NULL;
	}
}

/* Extract the node with the lowest key
 * @param [in] The heading node of the linked list
 * @return the node with the lowest key
 */
struct ListNode *extract_min_node(struct ListNode *head)
{
	struct ListNode *node = head->next;

	if (node == NULL)
		return NULL;
	head->next = head->next->next;
	return node;
}

/* Print the content of linked list (for debugging only)
 * param [in] head The heading node of the linked list
 */
void DebugLinkedList(struct ListNode *head)
{
	struct ListNode *cur = NULL;

	if (head == NULL || head->next == NULL)
		return;
	cur = head->next;
	pr_debug("List: ");
	while (cur != NULL) {
		pr_debug(" %d", cur->index);
		cur = cur->next;
	}
	pr_debug("\n");
}

#endif
